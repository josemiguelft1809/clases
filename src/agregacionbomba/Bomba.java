/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacionbomba;

/**
 *
 * @author Jose Figueroa
 */
public class Bomba {
    private int numBomba;
    private float capacidad;
    private float contadorLitros;
    private Gasolina gasolina;
    
    public Bomba(){
        this.numBomba = 0;
        this.capacidad = 0.0f;
        this.contadorLitros = 0;
        this.gasolina = new Gasolina();
    }

    public Bomba(int numBomba, float capacidad, float contadorLitros, Gasolina gasolina) {
        this.numBomba = numBomba;
        this.capacidad = capacidad;
        this.contadorLitros = contadorLitros;
        this.gasolina = gasolina;
    }
    
    public Bomba(Bomba x){
        this.numBomba=x.numBomba;
        this.capacidad=x.capacidad;
        this.contadorLitros=x.contadorLitros;
        this.gasolina=x.gasolina;
    }

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public float getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(float capacidad) {
        this.capacidad = capacidad;
    }

    public float getContadorLitros() {
        return contadorLitros;
    }

    public void setContadorLitros(float contadorLitros) {
        this.contadorLitros = contadorLitros;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }
    
    public float obtenerInventacio(){
        return this.capacidad - this.contadorLitros;
    }
    
public Boolean realizarVenta(float cantidad) {
    Boolean exito = false;
    if (cantidad <= this.obtenerInventacio()){
            exito = true;
        } return exito;
    }

    public float calcularTotalVenta(){
        return this.contadorLitros * this.gasolina.getPrecio();
    }
}
